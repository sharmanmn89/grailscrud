import org.example.SecRole

class BootStrap {

    def init = { servletContext ->
        def userRole = SecRole.findOrSaveByAuthority('ROLE_USER') ?: new SecRole(authority: 'ROLE_USER').save(failOnError: true)
        def adminRole = SecRole.findOrSaveByAuthority('ROLE_ADMIN') ?: new SecRole(authority: 'ROLE_ADMIN').save(failOnError: true)
    }
    def destroy = {
    }
}