package org.example

import GrailsCore.Book
import GrailsCore.FileUpload
import com.org.grailscore.CO.UserCO

class SecUser {

    transient springSecurityService

        Integer id
        String name
        String age
        String email
        String password
        String otp
        boolean enabled
        boolean accountExpired
        boolean accountLocked
        boolean passwordExpired

    static hasMany = [book: Book]

    static constraints = {
        id size:1..5, blank: false
        name size: 5..25, blank: false, unique: true
        age min: "18"
        email email: true, blank: false, unique: true
        password blank: false
        otp size: 1..5, nullable: true, blank: true
    }

    static mapping = {
        table 'SecUser'

        version false
        password column: '`password`'
    }

    SecUser() {

    }

    SecUser(UserCO userCO) {
        this.id = userCO.id
        this.name = userCO.name
        this.age = userCO.age
        this.email = userCO.email
        this.password = userCO.password
        this.otp = userCO.otp
    }

    Set<SecRole> getAuthorities() {
        SecUserSecRole.findAllByUser(this).collect { it.role } as Set
    }

//    def beforeInsert() {
//        encodePassword()
//    }
//
//    def beforeUpdate() {
//        if (isDirty('password')) {
//            encodePassword()
//        }
//    }
//
//    protected void encodePassword() {
//        password = springSecurityService.encodePassword(password)
//    }
}