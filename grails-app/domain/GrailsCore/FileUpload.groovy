package GrailsCore

class FileUpload {

    static belongsTo = [book: Book]

    String uploadedFile

    static constraints = {
        uploadedFile(nullable: true)
    }

    static mapping = {
        table 'FileUpload'

        version false
    }
}
