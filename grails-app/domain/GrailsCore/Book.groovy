package GrailsCore

import com.org.grailscore.CO.BookCO
import org.example.SecUser

class Book {

    static belongsTo = [user: SecUser]
    Integer id
    String authorName
    String bookName
    String date
    String language

    static hasMany = [uploadedFiles: FileUpload]

    static constraints = {
        id size:1..5, blank: false, unique: true
        authorName size: 5..25, blank: false
        bookName size: 5..50, blank: false
        date size: 1..10, blank: false
        language size: 5..15, blank: false
    }

    static mapping = {
        table 'Book'

        version false
        id column: 'BookId'
        authorName column: 'AuthorName'
        bookName column: 'BookName'
        date column: 'Date'
        language column: 'Language'
    }

    Book(BookCO bookCO){
        this.id = bookCO.id
        this.bookName = bookCO.bookName
        this.authorName = bookCO.authorName
        this.language = bookCO.language
        this.date = bookCO.date
        this.user = bookCO.user
    }

    Book() {

    }
}
