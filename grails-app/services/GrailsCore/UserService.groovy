package GrailsCore

import com.org.grailscore.CO.UserCO
import grails.transaction.Transactional
import org.example.SecRole
import org.example.SecUser
import org.example.SecUserSecRole
import org.springframework.web.multipart.commons.CommonsMultipartFile

@Transactional
class UserService{

    def groovyPageRenderer

    def create(UserCO userCO){
        if(userCO){
            def role = SecRole.findByAuthority('Role_USER')
            userCO.password = encodePassword(userCO.password)
            String otp = generateOTP()
            userCO.otp = otp
            SecUser newUser = new SecUser(userCO)
            if(newUser.validate()){
                newUser.save(flush: true)
                SecUserSecRole.create(newUser,role)
                return newUser
            }
            else{
                newUser.errors.allErrors.each {
                    println it
                }
                return null
            }
        }
        else{
            return null
        }
    }

    def update(UserCO userCO){
        def user = SecUser.get(userCO.id)
        userCO.password = encodePassword(userCO.password)
        user.properties = userCO
        if(user.validate()){
            user.save(flush: true)
        }
        else{
            user.errors.allErrors.each {
                println it
            }
        }
        return user
    }

    def authenticateUser(UserCO userCO) {
        def user
        if(userCO.password){
            userCO.password = encodePassword(userCO.password)
            user = SecUser.findByEmailAndPassword(userCO.email,userCO.password)
        }
        else {
            user = SecUser.findByIdAndOtp(userCO.id,userCO.otp)
        }
        boolean condition = false
        if(user!= null){
            condition = true
            return [condition: condition,id: user.id]
        }
        return [condition: condition]
    }

    def read(int id){
        def user = SecUser.get(id)
        def decoded = decodePassword(user.password)
        String template = groovyPageRenderer.render(template: "/user/update",model: [user: user,password:decoded])
        return template
    }

    def signup(){
        String template = groovyPageRenderer.render(template: "/user/create")
        return template
    }

    def show(){
        List<SecUser> userList = SecUser.getAll()
        return [list:userList,count:SecUser.count()]
    }

    def delete(UserCO userCO){
        def user = SecUser.get(userCO.id)
        user.delete(flush: true)
    }

    def encodePassword(String password){
        String encodedText = Base64.getEncoder().encodeToString(password.getBytes("UTF-8"))
        return encodedText
    }

    def decodePassword(String encodeText){
        byte[] decodeArr = Base64.getDecoder().decode(encodeText)
        String decodeText = new String(decodeArr,"UTF-8")
        return decodeText
    }

    def saveOTP(String email){
        if(SecUser.findByEmail(email)){
            def user = SecUser.findByEmail(email)
            String otp = generateOTP()
            user.otp = otp
            user.properties = otp
            if(user.validate()){
                user.save(flush: true)
                return user
            }
            else{
                user.errors.allErrors.each {
                    println it
                }
                return null
            }
        }
        else{
            return null
        }
    }

    def generateOTP(){
        String numbers = "0123456789";
        Random randomMethod = new Random();
        char[] otp = new char[4];
        for (int i = 0; i < 4; i++)
        {
            otp[i] = numbers.charAt(randomMethod.nextInt(numbers.length()));
        }
        return new String(otp)
    }

    def upload(Integer id){
        String template = groovyPageRenderer.render(template: "/user/fileUpload",model: [id: id])
        return template
    }

    def enterEmail(){
        String template = groovyPageRenderer.render(template: "/user/otpGenerator")
        return template
    }

    def fileUpload(def newFile,CommonsMultipartFile uploadedFile){
        def fileName = uploadedFile.originalFilename
        if (newFile.empty) {
            flash.message = 'file cannot be empty'

            return
        }
        newFile.transferTo(new File('/home/ongraph/Desktop/Uploaded File/'+fileName))
    }
}