package GrailsCore

import com.org.grailscore.CO.BookCO
import grails.transaction.Transactional
import com.org.grailscore.DataTableDto
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.example.SecUser
import org.springframework.web.multipart.commons.CommonsMultipartFile

@Transactional
class BookService {

    def groovyPageRenderer

    UserService userService

    def create(BookCO bookCO){
        if(bookCO.bookName!=null && bookCO.authorName!=null){
            def book = Book.findByBookNameAndAuthorName(bookCO.bookName,bookCO.authorName);
            if(book){
                return null
            }
            else{
                SecUser user = SecUser.get(bookCO.userId)
                bookCO.user = user
                Book newBook = new Book(bookCO)
                if(newBook.validate()){
                    newBook.save(flush: true)
                    return newBook
                }
                else{
                    newBook.errors.allErrors.each {
                        println it
                        return null
                    }
                }
            }
        }
    }

    def update(BookCO bookCO){
        if(bookCO.bookName!=null && bookCO.authorName!=null) {
            def book = Book.get(bookCO.id)
            bookCO.user = book.user
            book.properties = bookCO
            if (book.validate()) {
                book.save(flush: true)
            } else {
                book.errors.allErrors.each {
                    println it
                }
            }
            return book
        }
    }

    def read(int id){
        def book = Book.get(id)
        String template = groovyPageRenderer.render(template: "/book/updateBook",model: [book:book])
        return template
    }

    def get(Serializable id){
        def book = Book.get(id)
        return book
    }

    def show(){
        List<Book> bookList = Book.getAll()
        return [list:bookList,count:Book.count()]
    }

    def delete(int id){
        def book = Book.get(id)
        book.delete(flush: true)
    }

    def addBook(int id){
        String template = groovyPageRenderer.render(template: '/book/addBook',model: [id: id])
        return template
    }

    DataTableDto<List<List<String>>> showByUser(def id){
        SecUser user = SecUser.findById(id)
        List<Book> books = Book.findAllByUser(user)
        List<List<String>> bookResponse = []
        books.eachWithIndex{ book, index ->
            List<String> Data = []
            Data << book.bookName
            Data << book.authorName
            Data << book.language
            Data << book.date
            Data << book.id
            bookResponse << Data
            Data = []
        }
        return new DataTableDto<List<List<String>>>(data: bookResponse)
    }

//    static String formatDate(String dateInString) {
//        Date date = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy").parse(dateInString)
//        return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(date)
//    }

    def uploadTemplate(Integer id){
        String template = groovyPageRenderer.render(template: '/book/uploadFile',model: [bookId: id])
        return template
    }

    def uploadFile(def newFile,GrailsParameterMap params){
        CommonsMultipartFile uploadedFile = params.uploadedFile
        def fileName = uploadedFile.originalFilename
        def book = Book.get(params.bookId)
        if(FileUpload.findByBookAndUploadedFile(book,fileName)){
            return null
        }
        else {
            params.book = book
            params.uploadedFile = fileName
            def fileUploaded = new FileUpload(params)
            if (fileUploaded.validate()) {
                fileUploaded.save(flush: true)
            } else {
                fileUploaded.errors.allErrors.each {
                    println it
                }
            }
            if (newFile.empty) {
                flash.message = 'file cannot be empty'
                render view: "uploaded"
                return
            }
            def storagePath = '/home/ongraph/Desktop/Uploaded File/book'+fileUploaded.book.bookName
            def storagePathDirectory = new File(storagePath)
            if (!storagePathDirectory.exists()) {
                storagePathDirectory.mkdirs()
            }
            newFile.transferTo(new File('/home/ongraph/Desktop/Uploaded File/book'+fileUploaded.book.bookName+"/"+fileName))
            return fileUploaded
        }
    }
}