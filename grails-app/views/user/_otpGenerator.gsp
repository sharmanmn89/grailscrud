<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.example.SecUser" %>
<div id="body">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><g:message code="default.login.label" default="User Login"/></h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div id="myForm">
                <g:form controller="user" action="generateOTP">
            <div class="modal-body">
            <label>
                <g:message code="default.email.label" default="Email-ID" /> :
            </label>
            <g:textField id="enterEmail" class="formFields" name="email" required=""/><br/>
            </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success"><g:message code="default.email.otp" default="Generate OTP" /></button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><g:message code="default.close.label" /></button>
                    </div>
        </g:form>
            </div>
        </div>
    </div>
</div>