<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><g:message code="default.error.label" default="Error"/></h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
                <div class="modal-body">
                    <div>
                        <h6 id="errorMessage"><em>Entered Email is not registered.</em></h6>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><g:message code="default.close.label" /></button>
                </div>
        </div>
</div>