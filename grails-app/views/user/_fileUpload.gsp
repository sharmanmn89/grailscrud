<%--
  Created by IntelliJ IDEA.
  User: ongraph
  Date: 05/08/22
  Time: 3:39 PM
--%>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><g:message code="default.upload.label" default="Upload File"/></h4>
<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<div class="form">
    <g:form controller="user" method="post" action="saveFile" enctype="multipart/form-data">
    <div class="modal-body">
<div>

        <label>
            Upload File
        </label>
        <input type="file" class="formFields" name="uploadedFile" required="" />
        <g:hiddenField name="id" value="${id}" />
</div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-success"><g:message code="default.submit.label" default="Submit" /></button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><g:message code="default.close.label" /></button>
    </div>
    </g:form>
</div>
    </div>
</div>