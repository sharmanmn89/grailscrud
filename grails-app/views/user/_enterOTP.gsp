<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.example.SecUser" %>
<html>
<head>
    <title>Verify Account</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.css"/>

    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>
</head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
<style>
#formFields {
    width: 80%;
    padding: 8px 20px;
    margin: 15px 8px;
    border: 2px solid #000000;
    box-sizing: border-box;
}

#errorMessage {
    text-align: center;
    color: red;
    margin-top: 20px;
}
</style>
<body>
<div class="card container mt-5 py-3" style="width:500px">
    <h2 class="text-center my-3"><g:message code="default.login.otp" /></h2>
<g:form url="[controller:'user', action:'validateUser']">
                    <label for="otp">
                        <g:message code="default.otp.label" default="Enter OTP"/>
                    </label>
                    <g:textField id="formFields" class="formFields" name="otp" maxlength="4" required=""/><br/>
                    <g:hiddenField name="id" value="${id}" />
    <div class="container text-center mt-3" >
    <button type="submit" class="btn btn-primary"><g:message code="default.submit.label" /></button>
    </div>
    <div>
        <h6  id="errorMessage">${message}</h6>
    </div>
</g:form>
</div>
</body>
</html>