package GrailsCore

import com.org.grailscore.CO.UserCO
import grails.converters.JSON
import org.springframework.web.multipart.commons.CommonsMultipartFile

class UserController {

    UserService userService

    def mailService

    def grailsLinkGenerator

    def login() {
    }

    def validateUser(UserCO userCO){
        if((userCO.email != null && userCO.password != null) || (userCO.id != null && userCO.otp !=null)) {
            def response = userService.authenticateUser(userCO)
            if(response.condition){
                redirect(action: "home",id: response.id)
            }
            else{
                if(userCO.password){
                    render(view: "login",model: [message: "wrong email or password"])
                }
                else {
                    render(template: "enterOTP",model: [id: userCO.id, message: "wrong OTP entered"])
                }
            }
        }
        else{
            redirect action: "login"
        }
    }

    def home(Integer id) {
        [userId: id]
    }

    def index(Integer id){
        String message = "OOPS! Record already exists"
        render(view: "home",model: [userId: id,msg: message])
    }

    def saveUser(UserCO userCO) {
        def response = userService.create(userCO)
        if(response){
            def location = grailsLinkGenerator.link(controller: 'user', action: 'enterOTP', absolute: true,id: response.id)
            def name = 'Click Here'
            def link =  "<a href='${location}' target='_blank'>${name}</a>"
            mailService.sendMail {
                to userCO.email
                subject message(code: 'default.email.subject')
                html "${message(code: 'default.email.body', args: [response.otp])}<br/>${link}"+"${message(code: 'default.email.link.body')}"
            }
            render(view: "login",model: [message: "User registered Successfully.OTP is sent to registered email to verify account."])
        }
        else{
            render(view: "login",model: [message: "User with entered email already exists"])
        }
    }

    def registerUser(){
        String template = userService.signup()
        def map = [form: template]
        render map as JSON
    }

    def editUser(int id){
        String template = userService.read(id)
        def map = [form: template]
        render map as JSON
    }

    def updateUser(UserCO userCO) {
        def user = userService.update(userCO)
        redirect (action: "home",id: userCO.id)
    }

    def deleteUser(UserCO userCO) {
        userService.delete(userCO)
        redirect action: "home"
    }

    def email() {
        def template = userService.enterEmail();
        def map = [form: template]
        render map as JSON
    }

    def generateOTP(String email) {
        def response = userService.saveOTP(email)
        if(response){
            def location = grailsLinkGenerator.link(controller: 'user', action: 'enterOTP',id: response.id , absolute: true)
            def name = 'Click Here'
            def link =  "<a href='${location}' target='_blank'>${name}</a>"
            mailService.sendMail {
                to email
                subject message(code: 'default.email.otp.subject')
                html "${message(code: 'default.email.otp.body', args: [response.otp])}<br/>${link}"+"${message(code: 'default.email.link.body')}"
            }
            render(view: "_enterOTP", model: [id: response.id, message: "Otp is sent to your entered email"])
        }
        else{
            render(view: "login",model: [message: "User with entered email does not exists"])
        }
    }

    def enterOTP(int id){
        render(template: "enterOTP",model: [id: id])
    }

    def logout(){
        render view: "/user/login"
    }

    def uploadFile(Integer id){
        String template = userService.upload(id)
        def map = [form: template]
        render map as JSON
    }

    def saveFile(){
        def newFile = request.getFile('uploadedFile')
        CommonsMultipartFile uploadedFile = params.uploadedFile
        userService.fileUpload(newFile, uploadedFile)
        redirect(controller: "user",action: "home",id: params.id)
    }
}