package GrailsCore

import com.org.grailscore.CO.BookCO
import com.org.grailscore.DataTableDto
import grails.converters.JSON
import org.example.SecUser
import org.springframework.web.multipart.commons.CommonsMultipartFile

class BookController {

    BookService bookService

    def addBook(int id) {
        String template = bookService.addBook(id)
        def map = [form: template]
        render map as JSON
    }

    def saveBook(BookCO bookCO){
        def response = bookService.create(bookCO)
        if(response!=null){
            redirect(controller: "user",action: "home",id: bookCO.user.id)
        }
        else{
            redirect(controller: "user",action: "index",id: bookCO.userId)
        }
    }

    def deleteBook(int bookId,int userId){
        bookService.delete(bookId)
        redirect(controller: "user",action: "home",id: userId)
    }

    def editBook(int bookId){
        String template = bookService.read(bookId)
        def map = [form: template]
        render map as JSON
    }

    def updateBook(BookCO bookCO){
        def book = bookService.update(bookCO)
        redirect(controller: "user",action: "home",id: params.user.id)
    }

    def showUserBooks(Integer id){
        DataTableDto<List<List<String>>> dataTableDTO = bookService.showByUser(id)
        render(contentType: 'text/json') {
            [
                    'draw'           : params.draw,
                    'recordsTotal'   : 0,
                    'recordsFiltered': 0,
                    'order'          : [[dataTableDTO.orderColumnIndex ?: 0, dataTableDTO.direction ?: 'asc']],
                    'data'           : dataTableDTO.data ?: []
            ]
        }
    }

    def uploadFile(Integer bookId){
        String template = bookService.uploadTemplate(bookId)
        def map = [form: template]
        render map as JSON
    }

    def saveFile(){
        def newFile = request.getFile('uploadedFile')
        def uploadFile = bookService.uploadFile(newFile,params)
        if(uploadFile){
            render(view: '/user/home',model: [userId: uploadFile.book.userId, msg: "File Uploaded Successfully"])
        }
        else {
            def book = bookService.get(params.bookId)
            render(view: '/user/home',model: [userId:book.userId , msg: "File already exists"])
        }
    }
}
