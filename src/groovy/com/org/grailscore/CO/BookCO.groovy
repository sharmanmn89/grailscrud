package com.org.grailscore.CO


import org.example.SecUser
import grails.validation.Validateable

@Validateable
class BookCO {

    Integer id
    String authorName
    String bookName
    String date
    String language
    Integer userId
    SecUser user
}
