package com.org.grailscore.CO

import grails.validation.Validateable

@Validateable
class UserCO {

    Integer id
    String name
    String age
    String email
    String password
    String otp
}
